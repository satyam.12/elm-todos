// function to show date and creator details
function showData() {
    // calling injectTags function
    injectTags();
    const creator = document.getElementById('creator-info');
    creator.innerHTML = "Created & Maintained By: Satyam Trivedi"
    const dateElement = document.getElementById('date');
    let today = new Date();
    let options = { weekday:'long',month:'short',day:'numeric'};
    dateElement.innerHTML = 'Today: '+today.toLocaleDateString('en-us',options);
  }

// function to inject required tags in page
function injectTags() {
    const divTag = document.getElementById('container')
    divTag.insertAdjacentHTML(
        'afterbegin',
        '<h5 id="creator-info"></h5></h5><h6 id="date"></h6><h1 class="app-title">todos</h1>'
    );
}

// calling showData function
showData();
