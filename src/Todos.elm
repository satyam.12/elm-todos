module Todos exposing (..)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


main : Program () Model Message
main =
    Browser.sandbox
        { init = init
        , view = view
        , update = update
        }


type alias Todo =
    { text : String
    , completed : Bool
    , editing : Bool
    }


type alias Model =
    { todos : List Todo
    , inputText : String
    }


init : Model
init =
    { todos = [], inputText = "" }


type Message
    = AddTodo
    | ClearTodo
    | EditTodo Int
    | RemoveTodo Int
    | ToggleTodoStatus Int
    | UpdateTodo Int
    | ChangeInput String
    | EditTodoInput Int String


addToList : String -> List Todo -> List Todo
addToList input todos =
    case input of
        "" ->
            todos

        _ ->
            todos
                ++ [ { text = input
                     , completed = False
                     , editing = False
                     }
                   ]


removeFromList : Int -> List Todo -> List Todo
removeFromList index list =
    List.take index list ++ List.drop (index + 1) list


toggleAtIndex : Int -> List Todo -> List Todo
toggleAtIndex indexToToggle list =
    List.indexedMap
        (\currentIndex todo ->
            if currentIndex == indexToToggle then
                { todo | completed = not todo.completed }

            else
                todo
        )
        list


editAtIndex : Int -> List Todo -> List Todo
editAtIndex indexToEdit list =
    List.indexedMap
        (\currentIndex todo ->
            if currentIndex == indexToEdit then
                { todo | editing = not todo.editing }

            else
                todo
        )
        list


editInputAtIndex : Int -> String -> List Todo -> List Todo
editInputAtIndex indexToEditInput inputText list =
    List.indexedMap
        (\currentIndex todo ->
            if currentIndex == indexToEditInput then
                { todo | text = inputText }

            else
                todo
        )
        list


updateAtIndex : Int -> List Todo -> List Todo
updateAtIndex indexToUpdate list =
    List.indexedMap
        (\currentIndex todo ->
            if currentIndex == indexToUpdate then
                { todo | editing = not todo.editing, text = todo.text }

            else
                todo
        )
        list


update : Message -> Model -> Model
update message model =
    case message of
        AddTodo ->
            { model
                | todos = addToList model.inputText model.todos
                , inputText = ""
            }

        RemoveTodo index ->
            { model | todos = removeFromList index model.todos }

        ClearTodo ->
            { model | todos = [] }

        ToggleTodoStatus index ->
            { model | todos = toggleAtIndex index model.todos }

        ChangeInput input ->
            { model | inputText = input }

        EditTodo index ->
            { model | todos = editAtIndex index model.todos }

        UpdateTodo index ->
            { model | todos = updateAtIndex index model.todos }

        EditTodoInput index input ->
            { model | todos = editInputAtIndex index input model.todos }


view : Model -> Html Message
view model =
    div [ class "container" ]
        [ Html.form [ class "elm-form", onSubmit AddTodo ]
            [ input
                [ type_ "text"
                , placeholder "What needs to be done?"
                , class "elm-todo-input"
                , value model.inputText
                , onInput ChangeInput
                ]
                []
            ]
        , button
            [ type_ "button"
            , id "btn"
            , style "width" "100%"
            , onClick ClearTodo
            ]
            [ text "Clear All" ]
        , br [] []
        , if List.isEmpty model.todos then
            p [] []

          else
            ul [ class "todo-list elm-todo-list" ] (List.indexedMap viewTodo model.todos)
        ]


viewTodo : Int -> Todo -> Html Message
viewTodo index todo =
    if not todo.editing then
        li
            [ class "todo-item" ]
            [ label [ class "tick elm-tick" ]
                [ p [ class "tick", onClick (ToggleTodoStatus index) ]
                    [ text
                        (if todo.completed then
                            "✓"

                         else
                            ""
                        )
                    ]
                ]
            , span
                [ style "text-decoration"
                    (if todo.completed then
                        "line-through"

                     else
                        "none"
                    )
                , id ("text" ++ todo.text)
                ]
                [ text todo.text ]
            , button [ type_ "button", class "edit-btn", onClick (EditTodo index) ] [ text "Edit" ]
            , label [ class "delete-todo elm-delete-todo" ] [ p [ onClick (RemoveTodo index) ] [ text "❌" ] ]
            ]

    else
        li
            [ class "todo-item" ]
            [ input
                [ type_ "text"
                , id ("text" ++ todo.text)
                , value todo.text
                , onInput (EditTodoInput index)
                ]
                []
            , button [ type_ "button", class "save-btn", onClick (UpdateTodo index) ] [ text "Save" ]
            , label [ class "delete-todo elm-delete-todo" ] [ p [ onClick (RemoveTodo index) ] [ text "❌" ] ]
            ]
